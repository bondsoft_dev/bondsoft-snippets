$('.js-ajax-form').submit(function(e) {
  e.preventDefault();
  var data = $(this).serialize();
  data.type = $(this).data('type');
  $messages = $(this).find('.result_ajax_form');
  $.ajax({
    type: 'POST',
    url: $(this).attr('action');
    data: data,
    success: function (data) {
      $messages.html('');
      $(data).appendTo($messages);
      $(this).find('.form-body').hide('fast');      
    }
});
});

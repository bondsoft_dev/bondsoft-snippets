<?php
/*
ЗАПОЛНИТЕ ЭТИ ПОЛЯ
*/
//Перевод полей с формы
$TRANSLATE = array(
  'somebody' => 'Имя',
  'oncetoldme' => 'Телефон',
  'page' => 'Страница'
);
//Имя сайта
$SITENAME = 'Iveco.arenarostov.ru';

//Сообщение для клиента
$message = 'Ваша заявка принята, мы свяжемся с вами в скором времени.';

//Заполнение полей - включение отправки в этот сервис
//ID инфоблока
$IBLOCK_ID = '3';

//Информация о телеграмме
$TELEGRAM = array(
  'TOKEN' => '',
  'CHAT_ID' => array(''),
);

//Информация об email
$EMAIL = array(
  'TO' => '',
  'FROM' => '',
);


/*
Начинается код
*/
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//Инициализация
$data = $_POST;
$messageToSite = '';

//Валидация
if (!empty($data['name']) || !empty($data['phone'])) {
    $message = '<div class="error">Произошла ошибка</div>';
    echo json_encode($message);
    die();
}

//Вызов обработчиков
if (!empty($IBLOCK_ID)) {
  SendToIBlock($IBLOCK_ID, $data, $TRANSLATE);
}
if (!empty($TELEGRAM['TOKEN']) && !empty($TELEGRAM['CHAT_ID'][0])) {
  SendFormTelegram($TELEGRAM, $data, $TRANSLATE);
}
if (!empty($EMAIL['FROM'])) {
  SendFormEmail($EMAIL, $data, $TRANSLATE);
}

$result = '<div class="success">' . $message . '</div>';
echo $result;

function SendToIBlock($info, $data, $TRANSLATE)
{
  CModule::IncludeModule('iblock');
  $el = new CIBlockElement;
  $str = '';
  foreach ($data as $k => $val) {
      $str .= '<b>'. $TRANSLATE[$k] .'</b>: ' . $val . '<br>';
  }

  $arLoadProductArray = Array(
    "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
    "IBLOCK_ID"      => $info,
    "NAME"           => $data['somebody'] . ' ' . date('H:i d.m.Y'),
    "ACTIVE"         => "Y",
    "PREVIEW_TEXT"   => $str,
    "PREVIEW_TEXT_TYPE"   => "html"
    );
  $el->Add($arLoadProductArray);
}



function SendFormTelegram($info, $data, $TRANSLATE)
{

    $text = '';
    foreach ($data as $k => $val) {
        $text .= $TRANSLATE[$k] .': ' . $val . PHP_EOL;
    }

    foreach ($info['CHAT_ID'] as $id) {

    $ch = curl_init();
    curl_setopt_array(
          $ch,
          array(
              CURLOPT_URL => 'https://api.telegram.org/bot' . $info['TOKEN'] . '/sendMessage',
              CURLOPT_POST => true,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_TIMEOUT => 10,
              CURLOPT_POSTFIELDS => array(
                  'chat_id' => $id,
                  'text' => $text,
              ),
          )
      );
    curl_exec($ch);
  }
}



function SendFormEmail($info, $data, $TRANSLATE)
{
    $to= $info['TO'];
    $fromEmail = $info['FROM'];

    $subject = "Письмо с сайта " . $SITENAME;
    $str = '';
    foreach ($data as $k => $val) {
        $str .= '<b>'. $TRANSLATE[$k] .'</b>: ' . $val . '<br>';
    }
    /* Для отправки HTML-почты вы можете установить шапку Content-type. */
    $headers= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";

    /* и теперь отправим из */
    mail($to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $str, "MIME-Version: 1.0\r\nContent-type: text/html; charset=utf-8\r\nFrom: {$fromEmail}\r\nReply-To: {$fromEmail}\r\nX-Mailer: {$fromEmail}\r\n");
}

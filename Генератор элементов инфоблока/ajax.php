<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
define("NOT_CHECK_PERMISSIONS", true);
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');

if ($_GET['action'] == 'getIblockList') {
    $iblocks = array();
    $rsBlock = CIBlock::GetList(
        $arOrder  = array("SORT" => "ASC"),
        $arFilter = array(
            "ACTIVE"    => "Y"
        ),
        true
    );
    while($arBlocks = $rsBlock->fetch()) {
        $iblocks[] = $arBlocks;
    }
    $sectionsArr = array();
    $rsSection = CIBlockSection::GetList(
        $arOrder  = array("SORT" => "ASC"),
        $arFilter = array(),
        false,
        $arSelect = array("ID", "NAME", "IBLOCK_ID", "CODE", "IBLOCK_SECTION_ID"),
        false
    );
    while($arSection = $rsSection->fetch()) {
        $sectionsArr[] = $arSection;
    }
    $out = array(
        'iblocks' => $iblocks,
        'sections' => $sectionsArr
    );
    echo json_encode($out);
}

if($_POST) {
    $ajaxData = $_POST;
    if ($ajaxData['action'] == 'addNew') {

        $elCode = strtolower(str_replace(' ', '-', $elCode));
        $names = array_filter(array_map('trim', explode(',', $ajaxData['names'])));
        $elImg = 0;
        if($ajaxData['typeImage'] == 'parent') {
            echo 'parent';
            $rsSection = CIBlockSection::GetList(
                $arOrder  = array("SORT" => "ASC"),
                $arFilter = array(
                    "IBLOCK_ID" => intval($ajaxData['iblock']),
                    "SECTION_ID" => intval($ajaxData['section']),
                ),
                false,
                $arSelect = array("ID", "IBLOCK_ID", "PICTURE"),
                false
            );
            while($arSection = $rsSection->fetch()) {
                $elImg = $arSection['PICTURE'];
            }
        } else {
            $elImg = CFile::MakeFileArray($ajaxData['imgLink']);
        }

        foreach ($names as $name) {
            $elName = str_replace('[%NAME%]', $name, $ajaxData['itemName']);
            $elCode = str2url($elName);
            $elText = str_replace('[%NAME%]', $name, $ajaxData['text']);
            $rsElement = new CIBlockElement;
            $arFields = array(
                "ACTIVE"            => "Y",
                "NAME"              => $elName,
                "CODE"              => $elCode,
                "IBLOCK_ID" => $ajaxData['iblock'],
                "IBLOCK_SECTION_ID" => $ajaxData['section'],
                "PREVIEW_PICTURE" => $elImg,
                "DETAIL_PICTURE" => $elImg,
                "DETAIL_TEXT" => $elText,
                "DETAIL_TEXT_TYPE" => 'html'
            );
            if(!$id = $rsElement->Add($arFields)) {
                echo "Error:" . $rsElement->LAST_ERROR;
            }
        }
    }

}


function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',  'ы' => 'y',   'ъ' => '',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}
function str2url($str) {
    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "-");
    return $str;
}
document.addEventListener('DOMContentLoaded', ()=>{
    const spoilersList = document.querySelectorAll('.spoiler-wrapper');
    if (spoilersList.length > 0) {
        for (let i = 0; i < spoilersList.length; i++) {
            const wrapper = spoilersList[i];
            let sTitle = wrapper.querySelector('.spoiler-title');
            let sBody = wrapper.querySelector('.spoiler-body');
            sTitle.addEventListener('click', (e)=>{
                e.stopPropagation();
                if (sBody.classList.contains('action')) {
                    return;
                }
                sBody.classList.add('action');
                setTimeout(() => {
                    sBody.classList.toggle('open');
                    setTimeout(() => {
                        sBody.classList.remove('action');
                    }, 1000);
                }, 100);
            });
        }
    }
});
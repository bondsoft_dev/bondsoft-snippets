<?php
//Указываем размеры экранов, на которые ориентируемся
$imgSizes = array('1920', '1600', '1366', '1240', '768');


//Выводим картинку. Отрезайженная картинка применяется для разрешений экрана до $size+70px по ширине
?>

<img class="mainslider-image" srcset="<?php foreach ($imgSizes as $size): ?>
  <?php
  $img = CFile::ResizeImageGet(
    $arItem['PREVIEW_PICTURE'],
    array('width' => $size, 'height' => '2000'),
    'BX_RESIZE_IMAGE_PROPORTIONAL',
    false,
    array(),
    false,
    85
  );
  echo $img['src'] . ' ' . ($size + 70) . 'w,'
  ?>
<?php endforeach; ?>"
 src="<?php echo $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="">

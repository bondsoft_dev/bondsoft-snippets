
<?
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
 
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

if( isset($_POST["def_town"]) && $_POST["def_town"] != "Выберите" ) {
	
	echo '<h1 style="text-align: center;">Установка началась</h1>';
	
	install_region_for_bitrix($_POST["def_town"]);
	echo '<h1 style="text-align: center;">Установка завершилась</h1>';
	echo '<h2 style="text-align: center; color: red;">открыть в редакторе файл install_region_for_bitrix/info.php и прочитать</h2>';
	
} else {
	
	echo '
		<head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
			<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
			
			<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

			</head><body>';
			
			
	echo '<h1 style="text-align: center;">Установка регионов, выберете регион по умолчанию</h1>';
	
	$data = json_decode( file_get_contents( $_SERVER['DOCUMENT_ROOT'] . '/install_region_for_bitrix/json_base_region.txt') , true );
	
	echo '<div style="text-align: center; margin-top: 40px;">
	<form method="POST" acttion="/install_region_for_bitrix/index.php">
	<select name="def_town" class="selectpicker" data-live-search="true" data-style="btn-danger">';
	
	echo '<option>Выберите</option>';
	
	foreach( $data as $arr ) {
	echo '<option value="'.$arr["CODE"].'">'.$arr["NAME"].'</option>';
	}
	
	
	echo '</select>
	<h2>Протокол поддоменов:</h2>
	<select name="HTTP_SUB_PROTOCOL" class="selectpicker">
	<option value="http">http</option>
	<option value="https">https</option>
	</select>';
	
	if( file_exists( $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH .'/footer.php' ) ) {
		
	echo '<h2>Модифицировать '. SITE_TEMPLATE_PATH .'/footer.php автоматически?</h2>
	<select name="footer" class="selectpicker">
	<option value="0">НЕТ</option>
	<option value="1">ДА</option>
	</select>';	
	
	}
	
	echo '<input type="hidden" name="protocol" value="" id="protocol">
	<br><br><input type="submit" value="Начать установку" class="btn btn-success">
	</form></div>
	
					<script>
	$(".selectpicker2").selectpicker({
	  style: "btn-info",
	  size: 4
	});
	
	$(document).ready(function(){
	
		$("#protocol").attr("value",location.protocol);

	});

	</script>
	</body>';

	/* инфа по сайтам
	$rsSites = CSite::GetList($by="sort", $order="desc", Array());
	while ($arSite = $rsSites->Fetch())
	{
	  echo "<pre>"; print_r($arSite); echo "</pre>";
	}
	
	$rsTemplates = CSite::GetTemplateList("s1");
while($arTemplate = $rsTemplates->Fetch())
{
   $result[]  = $arTemplate;
}
echo "<pre>"; print_r($result); echo "</pre>";


$rsSites = CSite::GetByID("s1");
$arSite = $rsSites->Fetch();
echo "<pre>"; print_r($arSite); echo "</pre>";

echo SITE_TEMPLATE_PATH;

*/


	
	
}

function install_region_for_bitrix($def_town) {
global $DB;
CModule::IncludeModule('iblock');

/*

	Создаем тип инфоблока Регионы
	
*/
$arFields = Array(
    'ID'=>'region',
    'SECTIONS'=>'Y',
    'IN_RSS'=>'N',
    'SORT'=>100,
    'LANG'=>Array(
        'en'=>Array(
            'NAME'=>'Region',
            ),
		'ru'=>Array(
            'NAME'=>'Регионы',
            )
        )
    );
	
$obBlocktype = new CIBlockType;
$DB->StartTransaction();
$res = $obBlocktype->Add($arFields);
if(!$res)
{
   $DB->Rollback();
   echo 'Error: '.$obBlocktype->LAST_ERROR.'<br>';
   die();
}
else $DB->Commit();

/*

	Создаем инфоблок
	
*/

$ib = new CIBlock;
$arFields = Array(
  "ACTIVE" => "Y",
  "NAME" => "РЕГИОНЫ",
  "CODE" => "REGION",
  "LIST_PAGE_URL" => "",
  "DETAIL_PAGE_URL" => "",
  "IBLOCK_TYPE_ID" => "region",
  "SITE_ID" => Array("s1"),
  "SORT" => 500,
  "PICTURE" => array(),
  "DESCRIPTION" => "",
  "DESCRIPTION_TYPE" => "html",
  "GROUP_ID" => Array()
  );

$IBLOCK_ID = $ib->Add($arFields);


/*

	Свойства для инфоблока
	
*/
$ibp = new CIBlockProperty;


$arFields = Array(
  "NAME" => "Город",
  "ACTIVE" => "Y",
  "SORT" => "1",
  "CODE" => "TOWN",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_13 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "В городе",
  "ACTIVE" => "Y",
  "SORT" => "2",
  "CODE" => "TOWN_V",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_5 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "По городу",
  "ACTIVE" => "Y",
  "SORT" => "3",
  "CODE" => "TOWN_PO",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_6 = $ibp->Add($arFields);


$arFields = Array(
  "NAME" => "Область",
  "ACTIVE" => "Y",
  "SORT" => "4",
  "CODE" => "REGION",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_1 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "По области/по краю (без предлога)",
  "ACTIVE" => "Y",
  "SORT" => "5",
  "CODE" => "REGION_PO",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_10 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "В области/в крае (без предлога)",
  "ACTIVE" => "Y",
  "SORT" => "6",
  "CODE" => "REGION_V",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_11 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "Численность населения",
  "ACTIVE" => "Y",
  "SORT" => "7",
  "CODE" => "NUMBER_OF_CIVILIANS",
  "PROPERTY_TYPE" => "N",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_2 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "Координаты на карте",
  "ACTIVE" => "Y",
  "SORT" => "8",
  "CODE" => "MAP_COORDS",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_7 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "ID региона в Яндекс",
  "ACTIVE" => "Y",
  "SORT" => "9",
  "CODE" => "REGION_ID_Y",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_3 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "Соседние города",
  "ACTIVE" => "Y",
  "SORT" => "10",
  "CODE" => "NEIGHBORHOODS",
  "PROPERTY_TYPE" => "E",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_4 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "Вебмастер UIN",
  "ACTIVE" => "Y",
  "SORT" => "11",
  "CODE" => "WEBMASTER_VERIFICATION_UIN",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_8 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "Вебмастер хост ID",
  "ACTIVE" => "Y",
  "SORT" => "12",
  "CODE" => "WEBMASTER_HOST_ID",
  "PROPERTY_TYPE" => "S",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_9 = $ibp->Add($arFields);

$arFields = Array(
  "NAME" => "Зум карты на странице контактов",
  "ACTIVE" => "Y",
  "SORT" => "13",
  "CODE" => "MAP_ZOOM",
  "PROPERTY_TYPE" => "N",
  "IBLOCK_ID" => $IBLOCK_ID
  );

$PropID_12 = $ibp->Add($arFields);



/****************************************************************

	ЗАГРУЖАЕМ ДАННЫЕ С РЕГИОНАМИ
	
****************************************************************/
$data = json_decode( file_get_contents( $_SERVER['DOCUMENT_ROOT'] . '/install_region_for_bitrix/json_base_region.txt') , true );


/****************************************************************

	Подготовка массива для ввода
	
****************************************************************/

$new = array();
foreach($data as $arr) {
$new[]["data"] = $arr;
}

/****************************************************************

	ШАБЛОН ДЛЯ ЗАПИСИ СТРАНИЦ
	
****************************************************************/
	
$TEMPLATE_PAGES = array();

$TEMPLATE_PAGES[] = array( 	"NAME" => "NAME",
							"position" => array( "NAME" )
						);
						
$TEMPLATE_PAGES[] = array( 	"NAME" => "CODE",
							"position" => array( "CODE" )
						);
						
$TEMPLATE_PAGES[] = array( 	"NAME" => "ACTIVE",
							"position" => "N"
						);
						
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_1,
							"position" => array( "REGION" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_2,
							"position" => array( "NUMBER_OF_CIVILIANS" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_3,
							"position" => array( "REGION_ID_Y" ),
							"type" => "PROPERTY_VALUES"
						);

$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_5,
							"position" => array( "REGION_V" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_6,
							"position" => array( "REGION_PO" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_7,
							"position" => array( "MAP_COORDS" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_8,
							"position" => array( "WEBMASTER_VERIFICATION_UIN" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_9,
							"position" => array( "WEBMASTER_HOST_ID" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_10,
							"position" => array( "PO_OBL" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_11,
							"position" => array( "V_OBL" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_12,
							"position" => array( "MAP_ZOOM" ),
							"type" => "PROPERTY_VALUES"
						);
$TEMPLATE_PAGES[] = array( 	"NAME" => $PropID_13,
							"position" => array( "NAME" ),
							"type" => "PROPERTY_VALUES"
						);


// только для регионов
$res = add_pages_automat( $new , $IBLOCK_ID , 0 , $TEMPLATE_PAGES, $def_town );
																		


/****************************************************************

	Создаем файлы интерфейса
	
****************************************************************/

// Файл инициализации
add_end_file('init.php','/bitrix/php_interface/init.php',$def_town,$IBLOCK_ID,$_POST["protocol"],$_POST["HTTP_SUB_PROTOCOL"]);

// Класс для работы регионов
if( !is_dir($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/classes') )
mkdir($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/classes');
load_save_file('bs_region_domains.php','/bitrix/php_interface/classes/bs_region_domains.php',$def_town,$IBLOCK_ID,$_POST["protocol"],$_POST["HTTP_SUB_PROTOCOL"]);

// папка для кеша регионов
if( !is_dir($_SERVER['DOCUMENT_ROOT'] . '/bs_data') )
mkdir($_SERVER['DOCUMENT_ROOT'] . '/bs_data');

// Карта сайта
load_save_file('region_sitemap.php','/region_sitemap.php',$def_town,$IBLOCK_ID,$_POST["protocol"],$_POST["HTTP_SUB_PROTOCOL"]);

// ROBOTS
load_save_file('region_robots.php','/region_robots.php',$def_town,$IBLOCK_ID,$_POST["protocol"],$_POST["HTTP_SUB_PROTOCOL"]);

// Файл сброса кеша регионов
load_save_file('bs_regions_reset_cache.php','/bs_regions_reset_cache.php',$def_town,$IBLOCK_ID,$_POST["protocol"],$_POST["HTTP_SUB_PROTOCOL"]);


// Модификация .htaccess
$htaccess = file_get_contents( $_SERVER['DOCUMENT_ROOT'] . '/.htaccess' );

if( strpos( $htaccess , 'RewriteEngine On' ) !== false ) {

	$handle = fopen( $_SERVER['DOCUMENT_ROOT'] . '/.htaccess.to_region', 'w+');
	fwrite($handle, $htaccess);
	fclose($handle);
	
	$htaccess = str_replace('RewriteEngine On','RewriteEngine On'. PHP_EOL .'RewriteRule ^robots.txt$ /region_robots.php [L]'. PHP_EOL .'RewriteRule ^sitemap(.*).xml$ /region_sitemap.php?param=$1 [L]'. PHP_EOL ,$htaccess);
	
	$handle = fopen( $_SERVER['DOCUMENT_ROOT'] . '/.htaccess', 'w+');
	fwrite($handle, $htaccess);
	fclose($handle);

	echo '<span style="color;red">Файл .htaccess модифицирован</span>: старый сохранили как .htaccess.to_region<br>';

}


if( file_exists( $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH .'/footer.php' ) && $_POST["footer"] == 1 ) {
		
	add_end_file('footer.php',SITE_TEMPLATE_PATH .'/footer.php',$def_town,$IBLOCK_ID,$_POST["protocol"],$_POST["HTTP_SUB_PROTOCOL"]);	
	
} else echo '<span style="color;red">footer.php <b>НЕ модифицирован</b></span>: внесите необходимые изменения!<br>';

echo '<h1>ГОТОВО!</h1>';


}


function add_end_file($file,$to,$def_town,$IBLOCK_ID,$protocol,$HTTP_SUB_PROTOCOL) {
	
	$data = file_get_contents( $_SERVER['DOCUMENT_ROOT'] . '/install_region_for_bitrix/'.$file);
	$data2 = file_get_contents( $_SERVER['DOCUMENT_ROOT'] . $to );
	
	if( substr_count($data2,'<?') > substr_count($data2,'?>') ) {
		
		$data = $data2 . PHP_EOL . $data;
		
	} else {
		
		$data = $data2 . PHP_EOL . "<?" . $data . "?>";
		
	}

	// указываем инфоблок
	$data = str_replace('[IBLOCK_ADD_ID]',$IBLOCK_ID,$data);
	// указываем основной регион
	$data = str_replace('[def_town]',$def_town,$data);
	
	$ex_domain		= explode('.',$_SERVER['HTTP_HOST']);
	$zone			= $ex_domain[ count($ex_domain) - 1 ];
	$domain_level_2	= $ex_domain[ count($ex_domain) - 2 ];
	
	$data = str_replace('[MAIN_DOMAIN]',		 $protocol.'//'.$domain_level_2.'.'.$zone,	$data);
	$data = str_replace('[SUBDOMAIN]',			 '.'.$domain_level_2.'.'.$zone,				$data);
	$data = str_replace('[PREG_SUBDOMAIN]',		 '.'.$domain_level_2.'\.'.$zone,			$data);
	$data = str_replace('[HTTP_PROTOCOL]',		 $protocol,									$data);
	$data = str_replace('[HTTP_SUB_PROTOCOL]',	 $HTTP_SUB_PROTOCOL,						$data);

	$handle = fopen($_SERVER['DOCUMENT_ROOT'] . $to, 'w+');
	fwrite($handle, $data);
	fclose($handle);
	
	echo '<span style="color;red">Файл модифицирован</span>: '.$to.'<br>';

}

function load_save_file($file,$to,$def_town,$IBLOCK_ID,$protocol,$HTTP_SUB_PROTOCOL) {
	
	$data = file_get_contents( $_SERVER['DOCUMENT_ROOT'] . '/install_region_for_bitrix/'.$file);

	// указываем инфоблок
	$data = str_replace('[IBLOCK_ADD_ID]',$IBLOCK_ID,$data);
	// указываем основной регион
	$data = str_replace('[def_town]',$def_town,$data);
	
	$ex_domain		= explode('.',$_SERVER['HTTP_HOST']);
	$zone			= $ex_domain[ count($ex_domain) - 1 ];
	$domain_level_2	= $ex_domain[ count($ex_domain) - 2 ];
	
	$data = str_replace('[MAIN_DOMAIN]',		 $protocol.'//'.$domain_level_2.'.'.$zone,	$data);
	$data = str_replace('[SUBDOMAIN]',			 '.'.$domain_level_2.'.'.$zone,				$data);
	$data = str_replace('[PREG_SUBDOMAIN]',		 '.'.$domain_level_2.'\.'.$zone,			$data);
	$data = str_replace('[HTTP_PROTOCOL]',		 $protocol,									$data);
	$data = str_replace('[HTTP_SUB_PROTOCOL]',	 $HTTP_SUB_PROTOCOL,						$data);

	$handle = fopen($_SERVER['DOCUMENT_ROOT'] . $to, 'w+');
	fwrite($handle, $data);
	fclose($handle);
	
	echo '<span style="color;red">Файл добавили</span>: '.$to.'<br>';

}



function add_pages_automat( $DATA , $IBLOCK_ID , $IBLOCK_SECTION_ID = 0 , $TEMPLATE_PAGES = array(), $def_town ) {
	
	if( !is_array($DATA) ) 											die('<h1>Укажите массив с данными</h1>');
	if( !is_numeric($IBLOCK_ID) || $IBLOCK_ID == 0 )				die('<h1>ID инфоблока должно быть число и больше нуля</h1>');

	// Добавляем секции
	CModule::IncludeModule('iblock');
	$el = new CIBlockElement;
	
	// Обходим страницы
	foreach( $DATA as $url => $arr ) {
		
			$arLoadProductArray = array(
			  "ACTIVE" => "Y",
			  "IBLOCK_ID" => $IBLOCK_ID,
			  "IBLOCK_SECTION_ID" => $IBLOCK_SECTION_ID,
			);
			
			// Обработка по шаблону
			foreach( $TEMPLATE_PAGES as $KEY => $TEMPLATE_ARR ) {
				
				if( !isset($TEMPLATE_ARR["NAME"]) )	die('<h1>Указание $TEMPLATE_ARR["NAME"] обязательно</h1>');
				$arLoadProductArray = to_process_the_template($arLoadProductArray,$arr,$TEMPLATE_ARR);
	
			}
			
			// Создаем символьный код
			if( !isset($arLoadProductArray["CODE"]) ) {
					
				//Транслитерируем имя
				$translitName =  Cutil::translit(trim($arLoadProductArray["NAME"]), "ru", ["replace_space" => "-", "replace_other" => "-"]);
					
				// Если такой же код есть допишем ему номер
				$countDiffCode = CIBlockSection::GetList([], ['CODE' => $translitName])->SelectedRowsCount();
				if ($countDiffCode > 0) $translitName .= $countDiffCode + 1;
					
				$arLoadProductArray["CODE"] = $translitName; // символьный код
				
			}
			
			
			/*
			
				врезка необходимая только при добавлении региона

			*/
			
			if( $arLoadProductArray["CODE"] == $def_town ) { // Если основной регион
				
				$arLoadProductArray["ACTIVE"] = "Y"; // включаем запись
				$arLoadProductArray["SORT"]   = 1;  // делаем первым
				
			}
			
			$ELEMENT_ID = $el->Add($arLoadProductArray);
			
			$DATA[$url]["data"]["ELEMENT_ID"] = $ELEMENT_ID;
			
			if(!$ELEMENT_ID)  echo $el->LAST_ERROR . '<br>';
		
	}
	
	return $DATA;

}





function to_process_the_template($arFields,$arr,$TEMPLATE_ARR) {
				
	if( !isset($TEMPLATE_ARR["type"]) ) 		$TEMPLATE_ARR["type"] = "standart";
	if( !isset($TEMPLATE_ARR["type_data"]) ) 	$TEMPLATE_ARR["type_data"] = "str";
				
	// получаем значение
	$val = "";
	if( is_array($TEMPLATE_ARR["position"]) ) {
					
		$pre = $arr["data"];
		foreach( $TEMPLATE_ARR["position"] as $name ) {
						
			$pre = $pre[$name];
					
		}
					
		$val = $pre;
					
	} else {

		$val = $TEMPLATE_ARR["position"];
					
	}
	
				
	if( is_array($val) && $TEMPLATE_ARR["type_data"] != "file_multi" ) die( '<h1>Неверно указан<br>$TEMPLATE_ARR["position"]<br>Вводные данные: <pre>'.var_dump($TEMPLATE_ARR).'</pre>Результат: <pre>'.print_r($val).'</pre><br>Должен быть не массив</h1>' );
	if( !is_array($val) && $TEMPLATE_ARR["type_data"] == "file_multi" ) die( '<h1>Неверно указан<br>$TEMPLATE_ARR["position"]<br>Вводные данные: <pre>'.print_r($TEMPLATE_ARR).'</pre>Результат: <pre>'.print_r($val).'</pre><br>Должен быть массив</h1>' );
	
	// ИЗМЕНИМ $val в соответсвии с "type_data"
				
	if( $TEMPLATE_ARR["type_data"] == "file" ) { // Изображение или файл file_multi
					
		$val = CFile::MakeFileArray( $val );
					
	} elseif( $TEMPLATE_ARR["type_data"] == "file_multi" ) { // множество файлов или изображений
					
		$files = array();
		foreach( $val as $id => $file ) {

			$files['n'.$id]['VALUE'] = CFile::MakeFileArray( $file );

		}
		
		$val = $files;
					
	} else { // Строка или число
					
		$val = $val;
					
	}
				
				
	// Определяем назначение
	// Мета данные

	if( $TEMPLATE_ARR["type"] == "IPROPERTY_TEMPLATES" ) { // Мета данные
				
		$arFields["IPROPERTY_TEMPLATES"][$TEMPLATE_ARR["NAME"]] = $val;
					
	} elseif( $TEMPLATE_ARR["type"] == "PROPERTY_VALUES" ) { // свойства
					
		$arFields["PROPERTY_VALUES"][$TEMPLATE_ARR["NAME"]] = $val;
					
	} elseif( $TEMPLATE_ARR["type"] == "standart" ) { // остальное
					
		$arFields[$TEMPLATE_ARR["NAME"]] = $val;
					
	}
	
	return $arFields;

}
?>




<?php 

include_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/php_interface/dbconn.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/php_interface/classes/bs_region_domains.php');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$SUB_DOMAIN = BsRegionFuncs::checkUrlForRegion($_SERVER['HTTP_HOST']);

if( $SUB_DOMAIN == '' )
	
	$_REGION = BsRegionFuncs::getRegionParams('[def_town]');
	
else
	
	$_REGION = BsRegionFuncs::getRegionParams($SUB_DOMAIN);
	
	
if( $_REGION ) {

	$file = $_GET['param'];
	$smapContent = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/sitemap".$file.".xml");

	if ($SUB_DOMAIN != "") {
		$regionHost = HTTP_SUB_PROTOCOL ."://" . $SUB_DOMAIN . SUBDOMAIN;
		
		$smapContent = str_replace(MAIN_DOMAIN, $regionHost, $smapContent);
	}

	header("Content-type: text/xml");
	print $smapContent;

} else { header("HTTP/1.0 404 Not Found"); echo 'ERROR 404'; }
?>
<?

define("PATH_TO_FILE", $_SERVER['DOCUMENT_ROOT'].'/bs_data/town_array.data');
define("PATH_TO_TABLE", $_SERVER['DOCUMENT_ROOT'].'/bs_data/table_string.data');
define("MAIN_DOMAIN", '[MAIN_DOMAIN]');
define("SUBDOMAIN", '[SUBDOMAIN]');
define("PREG_SUBDOMAIN", '/([^.]+)\[PREG_SUBDOMAIN]/is');
define("HTTP_PROTOCOL", '[HTTP_PROTOCOL]');
define("HTTP_SUB_PROTOCOL", '[HTTP_SUB_PROTOCOL]');

class BsRegionFuncs
{
	/**
	 * Проверка url на наличие поддомена региона
	 * */
	public static $pathToTownArray = PATH_TO_FILE;
	
	public static $mainUrl = MAIN_DOMAIN;

	public static $mainUrlForCookie = SUBDOMAIN;

	public static $townArray = array();

	public static $gmapsUserApiKey;

	public static function checkUrlForRegion($url) {
		$url = str_replace(array("http://", "https://", "www"), "", $url);
		$result = preg_match(PREG_SUBDOMAIN, $url, $matches);
		return $matches[1];
	}
	public static function createTownArrayFromIBLOCK() {
		
		if(CModule::IncludeModule('iblock')) {
			$arSelect = Array(
				'ID',
				'IBLOCK_ID',
				'NAME', 'CODE',
				'PROPERTY_TOWN',
				'PROPERTY_TOWN_V',
				'PROPERTY_TOWN_PO',
				'PROPERTY_REGION',
				'PROPERTY_REGION_PO',
				'PROPERTY_REGION_V',
				'PROPERTY_NUMBER_OF_CIVILIANS',
				'PROPERTY_MAP_COORDS',
				'PROPERTY_REGION_ID_Y',
				'PROPERTY_NEIGHBORHOODS',
				'PROPERTY_WEBMASTER_VERIFICATION_UIN',
				'PROPERTY_WEBMASTER_HOST_ID',
				'PROPERTY_MAP_ZOOM'	
			);
			$arFilter = Array(
				'IBLOCK_ID'=>[IBLOCK_ADD_ID],
				'ACTIVE'=>'Y'
			);

			$res = CIBlockElement::GetList(Array("PROPERTY_NUMBER_OF_CIVILIANS" => "DESC"), $arFilter, false, false, $arSelect);

			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				
				$regionCode = $arFields['CODE'];
				$arToSave[$regionCode]['ID'] 							= $arFields['ID'];
				$arToSave[$regionCode]['CODE'] 							= $arFields['CODE'];
				$arToSave[$regionCode]['NAME'] 							= trim($arFields['NAME']);
				$arToSave[$regionCode]['TOWN'] 							= trim($arFields['PROPERTY_TOWN_VALUE']);
				$arToSave[$regionCode]['TOWN_V'] 						= trim($arFields['PROPERTY_TOWN_V_VALUE']);
				$arToSave[$regionCode]['TOWN_PO'] 						= trim($arFields['PROPERTY_TOWN_PO_VALUE']);
				$arToSave[$regionCode]['REGION'] 						= trim($arFields['PROPERTY_REGION_VALUE']);
				$arToSave[$regionCode]['REGION_PO'] 					= trim($arFields['PROPERTY_REGION_PO_VALUE']);
				$arToSave[$regionCode]['REGION_V'] 						= trim($arFields['PROPERTY_REGION_V_VALUE']);
				$arToSave[$regionCode]['REGION_ID_Y'] 					= trim($arFields['PROPERTY_REGION_ID_Y_VALUE']);
				$arToSave[$regionCode]['NUMBER_OF_CIVILIANS'] 			= trim($arFields['PROPERTY_NUMBER_OF_CIVILIANS_VALUE']);
				$arToSave[$regionCode]['MAP_COORDS'] 					= trim($arFields['PROPERTY_MAP_COORDS_VALUE']);
				$arToSave[$regionCode]['WEBMASTER_VERIFICATION_UIN'] 	= trim($arFields['PROPERTY_WEBMASTER_VERIFICATION_UIN_VALUE']);
				$arToSave[$regionCode]['WEBMASTER_HOST_ID'] 			= trim($arFields['PROPERTY_WEBMASTER_HOST_ID_VALUE']);
				$arToSave[$regionCode]['NEIGHBORHOODS'] 				= $arFields['PROPERTY_NEIGHBORHOODS_VALUE'];
				$arToSave[$regionCode]['MAP_ZOOM'] 						= $arFields['PROPERTY_MAP_ZOOM_VALUE'];

				$arToSaveNeighborhoods[$arFields['ID']]['NAME'] = trim($arFields['NAME']);
				$arToSaveNeighborhoods[$arFields['ID']]['CODE'] = trim($arFields['CODE']);
				$arToSaveNeighborhoods[$arFields['ID']]['TOWN'] = trim($arFields['PROPERTY_TOWN_VALUE']);
				$arToSaveNeighborhoods[$arFields['ID']]['MAP_COORDS'] = trim($arFields['PROPERTY_MAP_COORDS_VALUE']);
				$arToSaveNeighborhoods[$arFields['ID']]['LINK'] = 'https://' . trim($arFields['CODE']) . '' . self::$mainUrlForCookie;
			}
			//После формирования массива проходим по нему снова
			foreach ($arToSave as $key => $value) {
				//Проходим по каждому соседу и ищем его соответсвия
				foreach ($arToSave[$key]['NEIGHBORHOODS'] as $keyNeighborhood => $valueNeighborhood) {
					foreach ($arToSaveNeighborhoods as $keyToSaveNeighborhoods => $valueToSaveNeighborhoods) {
						if($valueNeighborhood == $keyToSaveNeighborhoods) {
							$arToSave[$key]['NEIGHBORHOODS'][$keyNeighborhood] = $valueToSaveNeighborhoods;
							break;
						}
					}
				}
			}
			$export = serialize($arToSave);
			file_put_contents(self::$pathToTownArray, $export);
			self::$townArray = $arToSave;
		}
	}
	public static function getSymbolCodesOfTowns() {

		if(file_exists(self::$pathToTownArray)) {
			
			$file = file_get_contents(self::$pathToTownArray);
			$array = unserialize($file);
			self::$townArray = $array;
			
		} else {
			self::createTownArrayFromIBLOCK();
		}
	}
	public static function getRegionParams($regionCode) {
		$arRegionParams = array();
		self::getSymbolCodesOfTowns();
		$data_array = self::$townArray;
		
		if( isset($data_array[$regionCode]) )
			return $data_array[$regionCode];
		else {
			
			self::createTownArrayFromIBLOCK();
			$data_array = self::$townArray;
			
			if( isset($data_array[$regionCode]) )
				return $data_array[$regionCode];
			else 
				return false;

		}

	}

	public static function getGmapsCoords($adress) {
		if( $curl = curl_init() ) {
		    curl_setopt($curl, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($adress) . '&key=' . self::$gmapsUserApiKey);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_POST, true);
		    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: 0"));
		    $json_responce = json_decode( curl_exec($curl) );
		    curl_close($curl);
		    // Если результат успешный, парсим ответ
		    if ( $json_responce->status == 'OK' ) {
		        $coord['lat'] = $json_responce->results[0]->geometry->location->lat;
		        $coord['lng'] = $json_responce->results[0]->geometry->location->lng;
		    }
		    return $coord;
		}
	}
	public static function Haversine($lat1,$lng1,$lat2,$lng2) {
		$deltaLat = $lat2 - $lat1 ;
		$deltaLng = $lng2 - $lng1 ;
		$earthRadius = 6372.8;
		$alpha = $deltaLat/2;
		$beta = $deltaLng/2;
		$a = sin(deg2rad($alpha)) * sin(deg2rad($alpha)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin(deg2rad($beta)) * sin(deg2rad($beta)) ;
		$c = 2 * atan2(sqrt($a), sqrt(1-$a));
		$distance = $earthRadius * $c;
		return $distance;
	}
	public static function addNeighboorhoodsOnMap($arrayWithDotsAndId) { //Содержит ['MAP_COORDS'] и ['ID']; Запись ведётся в ['NEIGHBORHOODS'];
		if(CModule::IncludeModule('iblock') && !empty($arrayWithDotsAndId)) {
		$tempResultArray = array();
		foreach ($arrayWithDotsAndId as $mainKey => $mainValue) {
			$mainDotCoords = explode(",", $mainValue['MAP_COORDS']);
			foreach ($arrayWithDotsAndId as $key => $value) {
				$secondDotCoords = explode(",", $value['MAP_COORDS']);
				if ($mainDotCoords[0] != $secondDotCoords[0] && $mainDotCoords[1] != $secondDotCoords[1]) {
					$tempResultArray[$mainValue['ID']][$value['ID']] = self::Haversine($mainDotCoords[0], $mainDotCoords[1], $secondDotCoords[0], $secondDotCoords[1]);
				}
			}
		}
			//Символьный код
			$PROPERTY_CODE = 'NEIGHBORHOODS';

			//Готовим данные для отправки в бд
			foreach ($tempResultArray as $key => $value) {
				//Сортируем по расстоянию
				asort($tempResultArray[$key]);
				//Выбираем ближайшие 5 городов
				$tempResultArray[$key] = array_slice($tempResultArray[$key], 0, 5, true);
				//Записываем ключи(id городов) в значение (вместо отдаленности) для записи в БД
				$tempResultArray[$key] = array_keys($tempResultArray[$key]);
				$PROPERTY_VALUE = $tempResultArray[$key];
				//Запись в БД
				CIBlockElement::SetPropertyValuesEx($key, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
			}
		}
	}
}
?>
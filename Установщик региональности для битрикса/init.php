

define("IBLOCK_TOWNS", [IBLOCK_ADD_ID]); //Инфоблок городов, для региональности
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/classes/bs_region_domains.php"))
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/classes/bs_region_domains.php");

global $SUB_DOMAIN;
$SUB_DOMAIN = BsRegionFuncs::checkUrlForRegion($_SERVER['HTTP_HOST']);
global $SUB_DOMAIN_EXIST_FL;
$SUB_DOMAIN_EXIST_FL = true;


//Выбираем город по умолчанию
$_REGION = BsRegionFuncs::getRegionParams('[def_town]');

if(!empty($SUB_DOMAIN)) {

	global $_REGION;

	if( $SUB_DOMAIN == '' )
		$_REGION = BsRegionFuncs::getRegionParams('[def_town]');
	else
		$_REGION = BsRegionFuncs::getRegionParams($SUB_DOMAIN);
	
	if($_REGION === false || $SUB_DOMAIN == "[def_town]" ) {
		
		header("HTTP/1.1 301 Moved Permanently"); 
		header("Location: " . MAIN_DOMAIN . $_SERVER['REQUEST_URI']);
	}
	
	// Вывод мета тега для яндекс вебмастера
	if( !empty($_REGION["WEBMASTER_VERIFICATION_UIN"]) )
	$APPLICATION->AddHeadString('<meta name="yandex-verification" content="'.$_REGION["WEBMASTER_VERIFICATION_UIN"].'" />',true);
	
}
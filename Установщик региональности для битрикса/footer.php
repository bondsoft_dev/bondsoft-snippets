
AddEventHandler("main", "OnEndBufferContent", "ChangeMyContent");

function ChangeMyContent(&$content)
{
	global $_REGION;
	
	$content = str_replace("[%TOWN%]", 			$_REGION['TOWN'], $content);
	$content = str_replace("[%TOWN_V%]", 		$_REGION['TOWN_V'], $content);
	$content = str_replace("[%TOWN_PO%]", 		$_REGION['TOWN_PO'], $content);
	$content = str_replace("[%REGION%]", 		$_REGION['REGION'], $content);
	$content = str_replace("[%REGION_V%]", 		$_REGION['REGION_V'], $content);
	$content = str_replace("[%REGION_PO%]", 	$_REGION['REGION_PO'], $content);
	$content = str_replace("[%YEAR%]", 			date("Y"), $content);

}


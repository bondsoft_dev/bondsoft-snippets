<?php 


include_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/php_interface/dbconn.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/php_interface/classes/bs_region_domains.php');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$SUB_DOMAIN = BsRegionFuncs::checkUrlForRegion($_SERVER['HTTP_HOST']);

if( $SUB_DOMAIN == '' )
	
	$_REGION = BsRegionFuncs::getRegionParams('[def_town]');
	
else
	
	$_REGION = BsRegionFuncs::getRegionParams($SUB_DOMAIN);
	
	
if( $_REGION ) {
		
		// Все изменения для региональных доменов - делаем относительно основного robots.txt через str_replace
		$robotsContent = file_get_contents("_robots.txt");
		
		//Добавляем заголовок чтобы результат был текстовый
		header('Content-Type: text/plain');

		if ( $SUB_DOMAIN != "" ) {
			$regionHost = HTTP_SUB_PROTOCOL ."://" . $SUB_DOMAIN . SUBDOMAIN;
			$regionSitemap = $regionHost . "/sitemap.xml";
			
			$robotsContent = str_replace( MAIN_DOMAIN, $regionHost, $robotsContent);
		} 
		
		print $robotsContent;

} else { header("HTTP/1.0 404 Not Found"); echo 'ERROR 404'; }


?>